package academic_system;

///CLASE GENERAL -- SISTEMA
class Sistema_Academico{
    Persona persona;
    public Sistema_Academico(){
        
    }
    void Cursos(){
        
    }
}

interface Persona {
    String Codigo_Persona,Direccion,Correo,Telefono,DNI;
    void Ver_alg();
};
class Profesor implements Persona{
    String Codigo_Profesor,Tipo_Contrato,Horas_Trabajo;
    Profesor profesor;
    public void Ver_alg(){
        String a;
    }
};
class Alumno implements Persona{
    public void Ver_alg()
    {
       String a;
    }
};

class Parcial_Final {
    
}

class Cursos{
    String Codigo_Curso,Nombre_Curso;
    void Ver_Contenido_Curso();
    void Ver_Pre_Requisitos();
    
}

//Interface para las ordenes
interface Orden{
    void Execute();
}
class Matricula implements Orden{
    public void Execute(){
        System.out.println("Alumno matriculado exitosamente... ");
    }
}
class Evaluacion implements Orden{  
    String Periodo_Academico;
    int Aprobo; //Debe ser bool, por mientras int 
    int Promedio;
    Persona persona;
    Evaluacion(Persona persona){
        this.persona = persona;
    }
    public void set_Detalles_Evaluacion()
    {
        //SE DEBERIA INGRESAR POR TECLADO LOS DETALLES PREGUNTANDO A LA PERSONA 
        //persona.Codigo_Profesor      = "131-10-28551";
        this.Periodo_Academico = "II";     
    }
    ///Metodo que implementa de la interface
    public void Execute(){
        ///Se llenan los detalles de la nueva evaluacion
        set_Detalles_Evaluacion();
        System.out.println("Persona creando nueva evaluacion... ");
    }   
};
class Nota implements Orden{
    public void Execute(){
        System.out.println("Nota agregada correctamente... ");
    }
}

//Clase que recibe ordenes y ejecuta el metodo execute() en cada orden.
class Invoker
{
    Orden Matricula;
    Orden Evaluacion;
    Orden Nota;
    public Invoker(Orden Matricula, Orden Evaluacion,Orden Nota){
        this.Matricula  =  Matricula;
        this.Evaluacion =  Evaluacion;
        this.Nota       =  Nota;
    } 
    public void Matricular()
    {
        Matricula.Execute();
    }
    public void Colocar_Nota(){
        Nota.Execute();
    }
    public void Crear_Evaluacion()    {
        Evaluacion.Execute();
    }
}